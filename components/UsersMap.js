import React from 'react'
import { View, StyleSheet } from 'react-native'
import MapView from 'react-native-maps'

const UsersMap = props => {
  let mapMarker = (props.userLocation ? (<MapView.Marker coordinate={props.userLocation} />) : null)

  // Draw users places markers
  let usersMarkers = props.usersPlaces.map(userPlace => <MapView.Marker coordinate={userPlace} key={userPlace.id} />)
  return (
    <View style={styles.mapContianer}>
        <MapView style={styles.map}
          initialRegion={{
          latitude: 37.78825,
          longitude: -122.4324,
          latitudeDelta: 0.0622,
          longitudeDelta: 0.0421,
          }}
          region={props.userLocation}>
          
          {mapMarker}
          {usersMarkers}
          </MapView>
    </View>
  )
}

export default UsersMap


const styles = StyleSheet.create({
    mapContianer: {
        width: '100%',
        height: 200,
        marginTop: 20
    },
    map: {
        width: '100%',
        height: '100%'
    }
})